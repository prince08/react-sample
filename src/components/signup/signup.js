import React from 'react';
import {TextField, Card, RaisedButton} from 'material-ui';
import {Link} from "react-router-dom";
import "../css/base.css"

const style = {
  btnStyle: {
    backgroundColor: '#00bcd4',
    width: 260,
  },
  headStyle: {
    color: "#00BCD4"
  },
  cardStyle: {
    width: 300,
    padding: 30,
    margin: 0,
  }
};

export default class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      confirmPassword: "",
    }
  }

  onChange = (field, value) => {
    this.state[field] = value;
    this.setState(this.state);
  };

  onSignp = () => {

  };

  render() {
    return (
      <div className={"alignCenter"} style={{padding: 74}}>
        <h2 style={style.headStyle}>Sign Up</h2>
        <div>
          <TextField
            floatingLabelText="Name"
          />
        </div>
        <div>
          <TextField
            floatingLabelText="Email"
          />
        </div>
        <div>
          <TextField
            type={"password"}
            floatingLabelText="Password"
          />
        </div>
        <div>
          <TextField
            type={"password"}
            floatingLabelText="Confirm Password"
          />
        </div>
        <div style={{marginTop: 20}}>
          Have an account? <Link to={"/login"}>Login here</Link>
        </div>
        <div className={"login-btn-container"}>
          <RaisedButton
            label="Signup" primary
            buttonStyle={style.btnStyle}
            onClick={this.onSignp}/>
        </div>
      </div>
    )
  }
}