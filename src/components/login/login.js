import React from 'react';
import {TextField, Card, RaisedButton} from 'material-ui';
import {Link} from "react-router-dom";
import "../css/base.css"

const style = {
  btnStyle: {
    backgroundColor: '#00bcd4',
    width: 260,
  },
  headStyle: {
    color: "#00BCD4"
  },
  cardStyle: {
    width: 300,
    padding: 30,
    margin: 0,
  }
};

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    }
  }

  onChange = (field, value) => {
    this.state[field] = value;
  };

  onLogin = () => {
    let {email = null, password = null} = this.state;
    /* here we can apply checks on email nd password*/
    if (email && password && email !== "" && password !== "") {
      /*
      perform api call for login here and on success response
      redirect to dashboard
       */
      this.props.history.push("dashboard")
    }else{
      /*show error for providing necessary values*/
      alert("Please fill all fields.")
    }
  };

  /**
   * we can customise our TextInput component so that we have not need to call onChange
   * again and again,
   * */

  render() {
    return (
      <div className={"alignCenter"} style={{padding: 74}}>
        <h2 style={style.headStyle}>Log in</h2>
        <div>
          <TextField
            onChange={(e) => {
              this.onChange("email", e.target.value)
            }}
            floatingLabelText="Email"
          />
        </div>
        <div>
          <TextField
            type={"password"}
            onChange={(e) => {
              this.onChange("password", e.target.value)
            }}
            floatingLabelText="Password"
          />
        </div>
        <div style={{marginTop: 20}}>
          Don't Have an account? <Link to={"/signup"}>Signup here</Link>
        </div>
        <div className={"login-btn-container"}>
          <RaisedButton
            label="Login" primary
            buttonStyle={style.btnStyle}
            onClick={this.onLogin}/>
        </div>
      </div>
    )
  }
}