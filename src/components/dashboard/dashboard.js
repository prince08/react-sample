import React, {Component} from 'react';
import {AppBar, Card} from 'material-ui';
import {get, isEmpty} from 'lodash';
import '../css/base.css';

export default class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cityInfo: {},
      weatherInfo: []
    }
  }

  componentWillMount(){
    navigator.geolocation.watchPosition((position) => {
      let {latitude, longitude} = position.coords;
      let key = "9a90f0f2794fb77cbf590b4fa4e0501d";
      let url = `http://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&APPID=${key}`;
      fetch(url).then(result => {
        return result.json();
      }).then(data => {
        console.log(data.list[0])
        this.setState({cityInfo: get(data, "city", {}), weatherInfo: get(data, "list", [])})
      }).catch(err => {
        console.log('---err-', err)
      })
    });
  }

  render() {
    let {cityInfo = {}, weatherInfo = []} = this.state;
    let cityName = !isEmpty(cityInfo) ? `${get(cityInfo, "name", "")}(${get(cityInfo, "country", "")})` : null;
    let rwStyle = {margin: 10, padding: 5, shadow: 5};
    return (
      <div>
        <AppBar
          className={"dashboard-header"}
          title={`${cityName ? cityName : ""}`}
          iconElementLeft={<div/>}
        />
        <table>
          <tbody>
          <tr>
            <td>Date(Time)</td>
            <td>Weather</td>
            <td>Wind(speed/deg)</td>
            <td>Temperature(in Kelvin)</td>
            <td>Humidity</td>
            <td>Pressure</td>
          </tr>
          {
            weatherInfo.map((info, index) => {
              let date = new Date(parseInt(get(info, "dt", 1)) * 1000);
              let time = date.toLocaleTimeString();
              date = date.toLocaleDateString();
              let weather = get(info, "weather[0].description", "");
              let wind = get(info, "wind", {});
              let temperature = get(info, "main.temp", "");
              let humidity = get(info, "main.humidity", "");
              let pressure = get(info, "main.pressure", "");

              return (
                <tr key={index} style={rwStyle}>
                  <td>{`${date} (${time})`}</td>
                  <td>{`${weather}`}</td>
                  <td>{`${get(wind, "speed", "")} / ${get(wind, "deg", "")}`}</td>
                  <td>{`${temperature}`}</td>
                  <td>{`${humidity}`}</td>
                  <td>{`${pressure}`}</td>
                </tr>
              )
            })
          }
          </tbody>
        </table>
      </div>
    )
  }
}